package com.meeting.jpa.entity;

import lombok.*;

import javax.persistence.*;
//import java.io.Serial;
import java.io.Serializable;
import java.sql.Time;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Participants implements Serializable {
   @Id

  private int participantId;


//    private   static  final long serialVersionUID=1L;
    private Time timeJoined;
    private Time timeExited;
    private Integer duration;
    private Boolean isPresent;
    private Integer assessmentScore;

    @ManyToOne
    @JoinColumn(name = "meeting_id")
    private Meeting meeting;


    @ManyToOne()
    @JoinColumn(name = "empEmailId")
    private Employee empEmailId;


}
