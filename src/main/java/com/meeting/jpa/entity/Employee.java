package com.meeting.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Employee implements Serializable {
@Id

    private int employeeId;
    private String email;

    private String firstName;
    private String lastName;
    private String projectCode;
    private String projectLocation;

    @OneToMany(mappedBy = "empEmailId",cascade = CascadeType.ALL)
    private List<Participants> participants;

    @OneToMany(mappedBy = "sme",cascade = CascadeType.ALL)
    private List<Meeting> meetings;

    @OneToMany(mappedBy = "employee",cascade = CascadeType.ALL)
    private List<SME> smes;

}
