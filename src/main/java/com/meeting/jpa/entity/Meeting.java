package com.meeting.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
public class Meeting implements Serializable {
    @Id

    private String meetingId;
    private Date meetingDate;
    private Integer duration;
    private Time startTime;
    private Time endTime;
    private String topic;

    @OneToMany( mappedBy = "meeting",cascade = CascadeType.ALL)
    private List<Participants> participants;

   @ManyToOne
   @JoinColumn(name = "Sme_id")
  private Employee sme;


}
