package com.meeting.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SME implements Serializable {

@Id
    private int sme_Id;



    private String subject;


    @ManyToOne
    @JoinColumn(name = "e_Id")
    private Employee employee;


}
