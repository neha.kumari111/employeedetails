package com.meeting.jpa.controller;

import com.meeting.jpa.dto.RequestDto;
import com.meeting.jpa.entity.Employee;
import com.meeting.jpa.entity.Meeting;
import com.meeting.jpa.entity.Participants;
import com.meeting.jpa.entity.SME;
import com.meeting.jpa.repository.MeetingRepository;
import com.meeting.jpa.repository.EmployeeRepository;
import com.meeting.jpa.repository.ParticipantsRepository;
import com.meeting.jpa.repository.SMERepository;
import com.meeting.jpa.service.MeetingService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class MeetingController {
    @Autowired
    private MeetingRepository meetingRepository;
    @Autowired
    private ParticipantsRepository participantsRepository;

   @Autowired
   private EmployeeRepository employeeRepository;

   @Autowired
   private SMERepository smeRepository;

   @Autowired
   private MeetingService meetingService;

    @PostMapping("/addMeeting")
    public Meeting addMeeting(@RequestBody RequestDto request){
       return this.meetingService.addMeeting(request);
    }

    @PostMapping("/addEmployee")
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @PostMapping("/addParticipants")
    public Participants createParticipants(@RequestBody Participants participants) {
        return participantsRepository.save(participants);
    }


    @PostMapping("/addSme")
    public SME createSME(@RequestBody SME sme) {
        SME sme1=new SME();
        sme1.setSme_Id(sme.getSme_Id());
        sme1.setSubject(sme.getSubject());
        Employee employee=sme.getEmployee();
        Employee savedemployee=employeeRepository.save(employee);
        System.out.println(savedemployee);
        sme1.setEmployee(savedemployee);
      return smeRepository.save(sme1);

    }

    @PostMapping("/meeting/{meetingId}/sme/{smeId}")
    public ResponseEntity<Meeting> allocateSMEToMeeting(@PathVariable String meetingId, @PathVariable int smeId) {
        Meeting meeting = meetingRepository.findById(meetingId).orElse(null);
        if (meeting == null) {
            return ResponseEntity.notFound().build();
        }

        Employee sme = employeeRepository.findById(smeId).orElse(null);
        if (sme == null) {
            return ResponseEntity.notFound().build();
        }

        Employee emp=new Employee();
        emp.setEmail(sme.getEmail());
        emp.setEmployeeId(sme.getEmployeeId());
        emp.setFirstName(sme.getFirstName());
        emp.setLastName(sme.getLastName());
        emp.setProjectLocation(sme.getProjectLocation());
        emp.setProjectCode(sme.getProjectCode());

        meeting.setSme(emp);


        meetingRepository.save(meeting);

        return ResponseEntity.ok(meeting);
    }
    @ApiOperation(value = "Get employee details by id" , notes = "Return the meeting details as per the id")


    @GetMapping("/findAllmeetings")
    public List<Meeting> findAllmeetings(){

        return this.meetingService.findAllmeetings();
    }

    @GetMapping("/meeting/{meetingId}/participants")
    public ResponseEntity<List<Participants>> getParticipantsByMeetingId(@PathVariable String meetingId) {

        return new ResponseEntity<>(this.meetingService.getParticipantsByMeetingId(meetingId), HttpStatus.OK);
    }

    @GetMapping("/getsmedetailsbymeetingId/{meetingId}")
    public Employee findSmeDeatailByMeetingId(@PathVariable String meetingId)
    {
        Optional<Meeting> meeting=meetingRepository.findById(meetingId);
        Meeting meeting1=meeting.get();
        Employee employee= meeting1.getSme();
        Employee employee1=new Employee();
        employee1.setEmployeeId(employee.getEmployeeId());
        employee1.setFirstName(employee.getFirstName());
        employee1.setLastName(employee.getLastName());
        employee1.setProjectLocation(employee.getProjectLocation());
        employee1.setProjectCode(employee.getProjectCode());
        employee1.setEmail(employee.getEmail());
        return employee1;
    }

    // User Story 4
    @PutMapping("/meeting/{meetingId}/sme/{smeId}")
    public ResponseEntity<Meeting> reallocateSMEsToMeeting(@PathVariable String meetingId, @RequestBody int smeIds) {
        Meeting meeting = meetingRepository.findById(meetingId).orElse(null);
        if (meeting == null) {
            return ResponseEntity.notFound().build();
        }
        //List<Employee> smes = new ArrayList<>();
        Optional<Employee> employee = employeeRepository.findById(smeIds);
        Employee emp =employee.get();
        if(emp==null){
            return ResponseEntity.notFound().build();
        }

        meeting.setSme(emp);
//
       // meeting.setSme((Employee) smes);
        Meeting savethemeeting= meetingRepository.save(meeting);

        return ResponseEntity.ok(savethemeeting);
    }
// user story 5
    @GetMapping("/meeting/{meetingId}/absentParticipants")
    public ResponseEntity<List<Participants>> getAbsentParticipantsByMeetingId(@PathVariable String meetingId) {
        try {
            List<Participants> absentParticipants = this.meetingService.getAbsentParticipantsByMeetingId(meetingId);
            return new ResponseEntity<>(absentParticipants, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
