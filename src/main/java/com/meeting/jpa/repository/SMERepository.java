package com.meeting.jpa.repository;

import com.meeting.jpa.entity.SME;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SMERepository extends JpaRepository<SME,Integer> {
}
